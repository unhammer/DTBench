{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Criterion.Main

import qualified Data.ByteString       as B
import qualified Data.ByteString.Char8 as C
import           Data.List             (permutations)
import qualified Data.Trie             as DT
import qualified Data.Trie.Internal    as DT
import qualified System.Environment

main = do
  d <- fileToDT "/tmp/scarrie-words"
  let inputs = map C.pack $ concatMap (take 1000 . permutations . C.unpack) $ take 1000 $ DT.keys d
  let queries = DT.fromList $ map (\p -> (p, ())) inputs
  putStrLn "isect:"
  print (DT.size d)
  defaultMain [ bgroup "isect" [ bench "isect 1k permutations of 1k words"  $ nf (isect d) queries
                               ]
              , bgroup "match" [ bench "match 1k permutations of 1k words"  $ nf (match d) inputs
                               ]
              ]

isect :: DT.Trie a -> DT.Trie a -> [C.ByteString]
isect d1 d2 =
      DT.keys $
        DT.intersectBy (\_ a -> Just a) d1 d2

match :: DT.Trie a -> [C.ByteString] -> [Maybe (C.ByteString, a, C.ByteString)]
match d1 = map (DT.match d1)

parseLine :: C.ByteString -> (C.ByteString, ())
parseLine l = case C.split '\t' l of
  []    -> ("", ())
  [  a] -> (a, ())
  a:b:_ -> (a, ())

fileToDT :: FilePath -> IO (DT.Trie ())
fileToDT file = do
  f <- B.readFile file
  let d = DT.fromList (map parseLine (C.lines f))
  print (DT.size d)
  return d


