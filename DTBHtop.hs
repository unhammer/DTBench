{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Data.ByteString       as B
import qualified Data.ByteString.Char8 as C
import qualified Data.Trie             as DT
import qualified System.Environment
import qualified System.Mem

main :: IO ()
main = htopWeigh "words.tsv"


htopWeigh :: FilePath -> IO ()
htopWeigh file = do
  d <- fileToDT file
  getLine >>= \a -> print (DT.lookup (C.pack a) d)
  System.Mem.performGC
  getLine >>= \a -> print (DT.lookup (C.pack a) d)
  System.Mem.performGC
  print $ DT.size d

parseLine :: C.ByteString -> (C.ByteString, DT.Trie ())
parseLine l = case C.split '\t' l of
  []    -> ("", DT.singleton "" ())
  [  a] -> (a, DT.singleton "" ())
  a:b:_ -> (a, DT.fromList $ map (\t -> (t, ())) (C.split '_' b))

fileToDT :: FilePath -> IO (DT.Trie (DT.Trie ()))
fileToDT file = do
  f <- B.readFile file
  let d = DT.fromList (map parseLine (C.lines f))
  print (DT.size d)
  return d


