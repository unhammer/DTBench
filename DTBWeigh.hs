{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Weigh

import           Control.Monad         (void)
import qualified Data.ByteString       as B
import qualified Data.ByteString.Char8 as C
import qualified Data.Trie             as DT
import qualified System.Environment
import qualified System.Mem
import           Text.Printf

main :: IO ()
main = do
  (r,c) <- Weigh.weighResults (do Weigh.io "weigh DT.fromList" veg "/tmp/scarrie-words")
  print r
  putStrLn "max MB:"
  print $ map (\a -> printf "%.3f" (fromIntegral (Weigh.weightMaxBytes $ fst a) / 1024 / 1024 :: Double) :: String) r

veg file = void $ fileToDT file

fileToDT file = do
  f <- B.readFile file
  let d = DT.fromList (map parseLine (C.lines f))
  print (DT.size d)
  return d

parseLine l = case C.split '\t' l of
  []    -> ("", DT.singleton "" ())
  [  a] -> (a, DT.singleton "" ())
  a:b:_ -> (a, DT.fromList $ map (\t -> (t, ())) (C.split '_' b))

