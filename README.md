# DTBench

```sh
$ stack build
$ stack exec weigh  # measure allocations and live bytes with Weigh
$ stack exec speed  # measure speed of intersectBy and match
$ stack exec memtop # to check memory usage in htop, type something to gc, type something to exit
```

## License
The word list is CC-BY 3.0, from
https://www.nb.no/sprakbanken/show?serial=oai:nb.no:sbr-9&lang=
converted with

`grep -e writtenForm -e synFeat  data/scarrie-lex-lmf.xml |awk -F'.*val="|" />' '/writtenForm/{if(f)print f; f=$2} /synFeat/{f=f"\t"$2}' >words.tsv `

